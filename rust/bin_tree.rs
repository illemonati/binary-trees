
struct BinTree<T: ToString> {
    left : Option<Box<BinTree<T>>>,
    right: Option<Box<BinTree<T>>>,
    content: T,
}


impl<T: ToString> BinTree<T> {
    fn new(content: T) -> BinTree<T> {
        BinTree {
            left: None,
            right: None,
            content: content
        }
    }
}

impl<T: ToString> ToString for BinTree<T> {
    fn to_string(&self) -> String {
        let mut res = String::new();
        if self.left.is_some() {
            res = format!("{}{}", res, (self.left.as_ref()).unwrap().to_string());
        }
        res = format!("{}{} ", res, self.content.to_string());
        if self.right.is_some() {
            res = format!("{}{}", res, self.right.as_ref().unwrap().to_string());
        }
        return res;
    }
}



fn main() {
    let mut tree = BinTree::new(1);
    tree.left = Some(Box::new(BinTree::new(2)));
    tree.right = Some(Box::new(BinTree::new(3)));
    println!("{}", tree.to_string());
}



