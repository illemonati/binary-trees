#include <iostream>
#include <inttypes.h>


template <typename T>
class BinTree {
public:
    BinTree<T> *left;
    BinTree<T> *right;
    T content;
    void print();
    BinTree(T *left, T content, T *right) : left(left), right(right), content(content) {}
    BinTree(T content) : left(NULL), right(NULL), content(content) {}
    ~BinTree() {
        delete this->left;
        delete this->right;
    }
};

template <typename T>
void BinTree<T>::print() {
    if (this->left) {
        this->left->print();
    }
    std::cout << this->content << " ";
    if (this->right) {
        this->right->print();
    }
}



int main() {

    BinTree<uint16_t> *tree = new BinTree<uint16_t>(1);
    tree->left = new BinTree<uint16_t>(2);
    tree->right = new BinTree<uint16_t>(3);

    tree->print();

    delete tree;


    return 0;
}






