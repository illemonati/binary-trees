#include<stdio.h>
#include<stdlib.h>


typedef struct BinTree BinTree;

struct BinTree {
    BinTree *left;
    BinTree *right;
    int content;
};

void print_bintree(BinTree *bt) {
    if (bt->left) {
        print_bintree(bt->left);
    }
    printf("%d ", bt->content);
    if (bt->right) {
        print_bintree(bt->right);
    }
}


BinTree * new_bintree(int content) {
    BinTree *tree = malloc(sizeof(BinTree));
    tree->content = content;
    tree->left = NULL;
    tree->right = NULL;
    return tree;
}

void free_bintree(BinTree * bt) {
    free(bt->left);
    free(bt->right);
    free(bt);
}




int main() {
    BinTree *tree = new_bintree(1);
    tree->left = new_bintree(2);
    tree->right = new_bintree(3);

    print_bintree(tree);

    free_bintree(tree);

    return 0;

}


